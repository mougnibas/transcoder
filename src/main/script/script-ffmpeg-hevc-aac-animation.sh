#!/bin/bash

# ==============================================================================
# Script Quickstart
# ==============================================================================
#
# First, map the input tracks to output tracks.
#
# The idea is to set the locale audio track as first audio track, and the other 
# track (original) as the second audio track.
#
# For the subtitles, the first one will the forced locale sub, then the 
# locale one, and finally the original one.
#
# Once the mapping is done, change the disposition according to the previous
# mapping (default tracks, forced tracks).
#
# Then, set the tracks language metadata (audio and subtitle).
#
# Transcode the video track (see below) as HEVC.
#
# Transcode the audio track(s) (see below) as AAC-LC.
#
# Finally, copy the subtitles as is.
#

# ==============================================================================
# Whole idea : Making high definition content available to home constraint
# ==============================================================================
#
# This specification is streaming oriented, with a bandwith limit up to
# 17Mbit/s.
#
# Video is quality constrained, with a upper bitrate limit up to 16Mbit/s.
#
# Maxium audio bitrate is 1024 kbit/s for 2 "7.1" tracks :
# 2 * "7.1" tracks * 512 kbit/s = 1 024 kbit/s
#
# Subtitles stream, chapters and metadata sizes are ignorable.
#

# ============================================================================= 
# HEVC / H.265 / libx265 Specific
# =============================================================================
#
# HEVC can achieve roughly 50% space gain compared to H.264, with same quality.
# This is especially true with HD (720p) and Full HD (1080p) content.
# 4K content is not tested yet.
#
# Based on my experiments and faulty vision, the following CRF (with default 
# "medium" preset) make visual experience reach "transparency" : CRF 23
# With a smaller CRF (better quality) beyound this point, I can't see the
# quality gain.
#
# A 64Mbit buffer is used.
#
# A "simple"  1080p scene at CRF 23 produce an average bitrate of 2.3Mbit/s.
# A "complex" 1080p scene at CRF 23 produce an average bitrate of 6.7Mbit/s.
# There is still room :)
#
# A single pass will do the job, as two pass quality gain is ignorable.
#
# Interlaced content MUST be deinterlaced before (double framerate) :
# -vf "yadif=mode=send_field:parity=tff|bff"
#
# Animation or heavily grain content can be tune :
# -tune grain|animation
#
# 10bit color depth is used.
#

# ==============================================================================
# AAC-LC/ ffmpeg aac internal Specific
# ==============================================================================
#
# According to my personnal tests (and faulty ears/setup/environment), 
# 64 kbit/s per channel is transparent, using ffmpeg aac internal and low
# complexity profile.
#
# Before 7.1 era, 5.1 "surround paired stereo" was "Back Left" and "Back Right".
# Now, 5.1 "surround paired stereo" are considered as "Side Left" and
# "Side Right", but aac specification don't know this channel mapping.
# Because of this, aac encoder will try to add program meta-data to map this
# layout, but it should not be use like this.
# Instead, we will simply say  :
# "Yeah it's a 5.1(side), but do it like it was plain old 5.1, because it's
# actually the case in old DD and DTS format".
# It will do the job.
# Syntax :
# -filter:a:0|1 aformat=channel_layouts="5.1"
#
# Bitrate to use, according to source track :
# - stereo (2 channels) : 128 kb/s
# - 5.1    (6 channels) : 384 kb/s
# - 7.1    (8 channels) : 512 kb/s
#
# Commons source bitrate are :
# - Dolby Digital @  192 kbit/s (2.0 system,  96 kbit/s per channel)
# - Dolby Digital @  384 kbit/s (2.0 system, 192 kbit/s per channel)
# - Dolby Digital @  448 kbit/s (5.1 system,  90 kbit/s per channel (LFE hack))
# - Dolby Digital @  640 kbit/s (5.1 system, 128 kbit/s per channel (LFE hack))
# - DTS           @  768 kbit/s (5.1 system, 128 kbit/s per channel)
# - DTS           @ 1536 kbit/s (5.1 system, 256 kbit/s per channel)
#
# References :
# - http://wiki.hydrogenaud.io/index.php?title=Fraunhofer_FDK_AAC
# - https://tech.ebu.ch/docs/tech/tech3324.pdf
#

# ==============================================================================
# Player and setup
# ==============================================================================
#
# Tested with :
# - "LePotato" board (AML-S905X-CC)
# - LibreELEC v9.0.1 (Kodi v18.1) (official "Le Potato" release)
# - Yamaha YAS-107 5.1 soundbar (Dolby Digital and DTS enabled decoder)
# - HDMI connection
#
# Kodi will "passthru" the audio stream to the soundbar receiver.
#
# I'm not sure about that, but this setup seem to read only the half of
# available bandwith in "player mode", resulting in unwatchable content because
# of empty buffer (with "forged" content to hit the upper bitrate limit).
# With a 4.5MiB/s available bandwith, the "player mode max speed" seems to
# be 2.5 MiB/s. 
#

# ==============================================================================
# Why no open format ?
# ==============================================================================
#
# Previous iteration was VP9 + Opus based.
#
# Opus format is great (transparency as 64kbit/s per channel, like AAC-LC).
# As it's an open format, software decoder is fine.
#
# VP9 format is fine too (almost same quality at same bitrate as HEVC), but
# libvpx rate control is just not fine at all to me.
# Let's take some simple example :
# - 1 pass @ CRF 31                       : average of 9.84Mbit/s
# - 1 pass @ CRF 31 @ 16Mbit/s constraint : average of 8.56Mbit/s
# Wait, what ? Why a never reached upper limit actually limit the bitrate ?
# Maybe a single pass issue. Let's try with 2 pass. The encoder will have the
# chance to first analyze the complexity to save bits or do proper things :
# - 2 pass @ CRF 31                       : average of  6.64 Mbit/s
# - 2 pass @ CRF 31 @ 16Mbit/s constraint : average of 11.92 Mbit/s
# Now, first case make little more sense. There is more time to analyze, so
# there is bit save. A difference of 3.2Mbit/s is quite strange, actually.
# But the second case make no sense to me. With an never reached upper limit,
# I get almost a double average bitrate.
# I know CRF is not a perfect quality requester, but I just can't understand
# libvpx one.
#
# Because of this, libvpx is not suitable to me anymore.
# libx265 rate control is much more simple/predictible/understandable, as long
# as the same preset is used with different CRF value.
#

# Reference content
REFERENCE=reference

# Media source content
MEDIA_SOURCE=DVD|Bluray

# ffmpeg command
ffmpeg                                                                         \
  -y                                                                           \
  -hide_banner                                                                 \
  -i $REFERENCE.mkv                                                            \
                                                                               \
  -map 0:0                                                                     \
  -map 0:2 -map 0:1                                                            \
  -map 0:5 -map 0:4 -map 0:3                                                   \
                                                                               \
  -map_metadata -1                                                             \
  -disposition:v:0 default                                                     \
  -disposition:a:0 default                                                     \
  -disposition:a:1 0                                                           \
  -disposition:s:0 forced                                                      \
  -disposition:s:1 0                                                           \
  -disposition:s:2 0                                                           \
                                                                               \
  -metadata:s:a:0 language=fre                                                 \
  -metadata:s:a:1 language=eng                                                 \
  -metadata:s:s:0 language=fre                                                 \
  -metadata:s:s:1 language=fre                                                 \
  -metadata:s:s:2 language=eng                                                 \
                                                                               \
  -codec:v libx265 -pix_fmt yuv420p10le                                        \
  -crf 23 -maxrate 16M -bufsize 64M -preset medium                             \
  -profile:v main10 -x265-params level-idc=5.1                                 \
  -tune animation                                                              \
                                                                               \
  -codec:a aac                                                                 \
  -b:a:0 384k -filter:a:0 aformat=channel_layouts="5.1"                        \
  -b:a:1 384k -filter:a:1 aformat=channel_layouts="5.1"                        \
  -aac_coder twoloop                                                           \
  -profile:a aac_low                                                           \
                                                                               \
  -codec:s copy                                                                \
                                                                               \
  $REFERENCE-$MEDIA_SOURCE-transcoded.mkv
