<!-- © Copyright 2018 Yoann MOUGNIBAS -->

<!-- This file is part of Transcoder. -->

<!-- Transcoder is free software: you can redistribute it and/or modify -->
<!-- it under the terms of the GNU General Public License as published by -->
<!-- the Free Software Foundation, either version 3 of the License, or -->
<!-- (at your option) any later version. -->

<!-- Transcoder is distributed in the hope that it will be useful, -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the -->
<!-- GNU General Public License for more details. -->

<!-- You should have received a copy of the GNU General Public License -->
<!-- along with Transcoder. If not, see <http://www.gnu.org/licenses/> -->

# Specification

## Whole idea : Making high definition content available to home constraint

This specification is streaming oriented, with a bandwith limit up to 17Mbit/s.

Video is quality constrained, with a upper bitrate limit up to 16Mbit/s.

Maxium audio bitrate is 1024 kbit/s for 2 "7.1" tracks :

`2 * "7.1" tracks * 512 kbit/s = 1 024 kbit/s`

Subtitles stream, chapters and metadata sizes are ignorable.

## Video

### Video encoder

HEVC can achieve roughly 50% space gain compared to H.264, with same quality.
This is especially true with HD (720p) and Full HD (1080p) content.
4K content is not tested yet.

A 64Mbit buffer is used.

A "simple"  1080p scene at CRF 23 produce an average bitrate of 2.3Mbit/s.
A "complex" 1080p scene at CRF 23 produce an average bitrate of 6.7Mbit/s.
There is still room :)

A single pass will do the job, as two pass quality gain is ignorable.

Interlaced content MUST be deinterlaced before :
`-vf "yadif=parity=tff|bff"`

Animation or heavily grain content can be tune :
`-tune grain|animation`

### Video encoder profile and level

Up main10, level 5.1

### Video PAR, SAR and framerate

Same as source.

### Video quality (CRF)

Based on my experiments and faulty vision, the following CRF (with default
"medium" preset) make visual experience reach "transparency" : CRF 23
With a smaller CRF (better quality) beyound this point, I can't see the
quality gain.

### Video maximum bitrate

16Mbit/s

## Audio

### Audio format

I've choose to transcode audio files rather than "pass-thru" them to hardware
decoders for two reasons :

  1. HD lossless formats (DTS-HD-MA and Dolby TrueHD) have maximum bitrate far
    too high (2.25-3 MiB/s) for home constrained network bandwidth
    (4.50 MiB/s available for ALL streams).
  2. I have doubts about support of them in the next decades by manufacturers,
     because of closed standard.

The drawback to this :  
Because there is no open specification of those formats, software decoders use
reverse engineering. I suppose "best efforts" are done, considering the close
nature of the formats, but decoding _may_ be inaccurate.  
Since I don't an ear able to distinguish the differences, this _potential_
quality loss is acceptable.

### Audio number of channels

Same as source. It will be the player's job to eventually downmix them to
match the current speaker setup.

### Audio encoder

AAC LC (low complexity profile).

According to my personnal tests (and faulty ears/setup/environment),
64 kbit/s per channel is transparent, with ffmpeg native aac and low complexity
profile.

Bitrate to use, according to source track :

 References :
 - http://wiki.hydrogenaud.io/index.php?title=Fraunhofer_FDK_AAC
 - https://tech.ebu.ch/docs/tech/tech3324.pdf

Commons source bitrate are :

```
- Dolby Digital @  192 kbit/s (2.0 system,  96 kbit/s per channel)
- Dolby Digital @  384 kbit/s (2.0 system, 192 kbit/s per channel)
- Dolby Digital @  448 kbit/s (5.1 system,  90 kbit/s per channel (LFE hack))
- Dolby Digital @  640 kbit/s (5.1 system, 128 kbit/s per channel (LFE hack))
- DTS           @  768 kbit/s (5.1 system, 128 kbit/s per channel)
- DTS           @ 1536 kbit/s (5.1 system, 256 kbit/s per channel)
```

### Audio tracks to include

Include original track.

Include locale track (if any).

Do NOT include "bonus" tracks (like comments).

### Audio bitrate

64 kbit/s per channel :

```
- stereo (2 channels) : 128 kb/s
- 5.1    (6 channels) : 384 kb/s
- 7.1    (8 channels) : 512 kb/s
```


### Default track

Default track : Locale track.

If not available, use original track

## Subtitles

Pass-thru the source subtitles, but mark them with correct metadata :

* language
* default yes/no
* forced yes/no

Do not burn forced subtitles into the video, because it's the player role to
automatically display them.

### Subtitles tracks to include

Include locale forced track (if any).

Include locale track (if any).

Include original track (if any).

Do NOT include bonus tracks (like comments).

### Subtitles formats

Supported formats :

  * PGS (bluray)
  * IDX/SUB (dvd)

### Default subtitle track

If there is no locale audio stream, then mark the locale subtitle as default.
Else, don't mark a subtitle track as default.

## Chapters

Include them, if any.
