<!-- © Copyright 2018 Yoann MOUGNIBAS -->

<!-- This file is part of Transcoder. -->

<!-- Transcoder is free software: you can redistribute it and/or modify -->
<!-- it under the terms of the GNU General Public License as published by -->
<!-- the Free Software Foundation, either version 3 of the License, or -->
<!-- (at your option) any later version. -->

<!-- Transcoder is distributed in the hope that it will be useful, -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the -->
<!-- GNU General Public License for more details. -->

<!-- You should have received a copy of the GNU General Public License -->
<!-- along with Transcoder. If not, see <http://www.gnu.org/licenses/> -->

# Implementation

## Requirement

  * (Unencrypted dvd or bluray source)
  * MKV source with orignal streams (video, audio(s) and subtitle(s)) 
  inside it
  * Audio lossless tracks ("DTS HD MA" and "Dolby TrueHD") previously 
  transcoded into FLAC.
  * docker image `mougnibas/ffmpeg` as transcoder

## Script

`script-ffmpeg-hevc-aac.sh`

## Docker image

TODO