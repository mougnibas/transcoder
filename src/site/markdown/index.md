<!-- © Copyright 2018 Yoann MOUGNIBAS -->

<!-- This file is part of Transcoder. -->

<!-- Transcoder is free software: you can redistribute it and/or modify -->
<!-- it under the terms of the GNU General Public License as published by -->
<!-- the Free Software Foundation, either version 3 of the License, or -->
<!-- (at your option) any later version. -->

<!-- Transcoder is distributed in the hope that it will be useful, -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the -->
<!-- GNU General Public License for more details. -->

<!-- You should have received a copy of the GNU General Public License -->
<!-- along with Transcoder. If not, see <http://www.gnu.org/licenses/> -->

# Introduction

## Purpose

This is a project to handle home transcoding rules (specification) of 
multimedia files.  

The file need to be readable in a constrained network and hardware environment.

## [Specification](specification.html)

The specification is a set of rules to apply, to get a right file.  

## [Implementation](implementation.html)

There is a default implementation of the specification.
