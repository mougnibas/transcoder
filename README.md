```
© Copyright 2018 Yoann MOUGNIBAS

This file is part of Transcoder.

Transcoder is free software: you can redistribute it and/or modif
it under the terms of the GNU General Public License as published
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Transcoder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Transcoder. If not, see <http://www.gnu.org/licenses/>
```

# Project informations

## General

A project to handle home transcoding rules (specification) with an 
implementation of it.

## Build

Build requirement: [Apache Maven](http://maven.apache.org/)

How to build ?

`mvn clean package` (for download purpose)

`mvn site` (for web purpose)
